const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Added browserSync for automatically update page after changes without F5 btn

mix.browserSync({
    proxy: process.env.VUE_APP_URL,
    middleware: require('serve-static')('public'),
}).js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
