export default {
    data: function () {
        return {
            languages: [],
            languages_urn: '/admin/pages/getFormData',
            loading_languages: false,
        }
    },
    methods: {
        getLanguagesRequest () {
            this.loading_languages = true;
            axios.get( this.languages_urn ).then( res => {
                if ( res.data.data && res.data.data.languages ) {
                    this.languages = res.data.data.languages
                }
            }).catch( e => {
                console.log('getFormDataError: ', e)
            }).then( _ => {
                this.loading_languages = false;
            })
        },
        getLanguage(id = null) {
            if (id) {
                for (let i = 0; i < this.languages.length; i++) {
                    if (this.languages[i].id === id) {
                        return this.languages[i]
                    }
                }
            }
            return {}
        }
    }
}
