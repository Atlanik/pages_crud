<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'home' );
Route::view('/news', 'news' );

function defaultApiRoutes () {
    Route::get('getAll','AdminPagesController@getAll');
    Route::get('getFormData','AdminPagesController@getFormData');

    Route::post('store','AdminPagesController@store');
    Route::post('update/{page_id}','AdminPagesController@update');

    Route::delete('destroy/{page_id}','AdminPagesController@destroy');
    Route::get('restore/{page_id}','AdminPagesController@restore');
    Route::delete('delete/{page_id}','AdminPagesController@delete');
}

// Admin routes
Route::group([
    'prefix' => 'admin/pages',
    'as' => 'admin.pages'
], function () {
    defaultApiRoutes();
});
Route::group([
    'prefix' => 'admin/news',
    'as' => 'admin.news'
], function () {
    defaultApiRoutes();
});
