<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRequest;
use App\Http\Requests\UpdateRequest;
use App\Http\Resources\DefaultResponse;
use Faker;

class AdminPagesController extends Controller
{
    private $pages = [];
    private $languages = [['sign' => 'en', 'name' => 'English' ]];
    public function __construct()
    {
        //Grate library for data generation in Laravel
        $faker = Faker\Factory::create();

        // generate languages
        for ( $i = 0 ; $i < rand(3, 8) ; $i ++ ) {
            $this->languages[] = [
                'sign' => $faker->languageCode,
                'name' => $faker->country,
            ];
        }

        // generate pages
        for ( $i = 0; $i <= rand(4, 10); $i ++ ){
            $randLang = $faker->randomElement($this->languages)['sign'];
            $this->pages[] = [
                'id'=> $i + 1,
                'title' => ['en' => $faker->sentence, $randLang => $faker->sentence],
                'slug' => $faker->unique()->word,
                'content' => ['en' => $faker->paragraph(), $randLang => $faker->paragraph],
                'author_id' => rand(12, 2000),
                'parent_id' => null,
                'status' => rand(0, 1) === 0,
                'created_at' => now()->format('Y-m-d H:i:s'),
                'updated_at' => now()->format('Y-m-d H:i:s'),
                'deleted_at' => rand(0, 1) === 1 ?  now()->toString(): null
            ];
        }
    }

    public function getAll()
    {
        return DefaultResponse::make($this->pages);
    }

    public function getFormData () {

        return DefaultResponse::make(['languages'=>$this->languages]);
    }

    public function store ( StoreRequest $request ) {
        return DefaultResponse::make($request->validated());
    }

    public function update ( $page_id, UpdateRequest $request ) {
        return DefaultResponse::make( array_merge($request->validated(), ['id' => $page_id]) );
    }

    public function destroy ( $id ) {
        return DefaultResponse::make([
            'id'=> $id,
            'deleted_at' => now(),
            'updated_at' => now()
        ]);
    }

    public function restore () {
        return DefaultResponse::make(['updated_at'=>now()]);
    }

    public function delete () {
        return DefaultResponse::make([]);
    }
}
