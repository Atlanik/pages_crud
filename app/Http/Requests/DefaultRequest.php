<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DefaultRequest
 * @package App\Http\Requests
 */
abstract class DefaultRequest extends FormRequest
{
    protected $rules = [
        'page_id' => [ 'integer', 'min:0', 'max:'.PHP_INT_MAX ],
        'slag'  => [ 'string' ], // also I think it`s field need unique rule
        'title' => [ 'string', 'min:4', 'max:255' ],
        'text'  => [ 'string' ],
        'status'=> [ 'boolean' ]
    ];

    abstract function initRules ();

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->initRules();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }
}
