<?php

namespace App\Http\Requests;

/**
 * Class DefaultRequest
 * @package App\Http\Requests
 */
class StoreRequest extends DefaultRequest
{
    public function initRules()
    {
        $this->rules['slug'][] = 'required';
        $this->rules['title'][] = 'required';
    }
}
