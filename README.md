## Vue components usage
Composer.json should have this dependencies
````
"dependencies": {
    ...
    "element-ui": "^2.13.1",
    "tiptap": "^1.27.1",
    "tiptap-extensions": "^1.29.1"
}
````
All files from ``resources/js/components`` copy to your components directory;

New component add in ``resources/js/app.js``
````js
Vue.component('pages-list-component', require('./components/pages/ListPagesComponent').default);
````

After it run commands
```
npm install
npm run prod
```

Now you can use tag ``<pages-list-component></pages-list-component>`` in your front-end
